from des import DesKey
class DES:
	"""
		Se usa la libreria DES
		puede encontrar detalles en la siguiente liga:
		https://pypi.org/project/des/
	"""
	key=''

	def cifrar(self,mensaje, key):
		cipher = DesKey(key.encode("utf-8")) 
		return cipher.encrypt(mensaje, padding=True)

	def descifrar(self, mensaje, key):
		cipher = DesKey(key.encode("utf-8"))
		return cipher.decrypt(mensaje, padding=True)

	def cifrar_archivo(self, nombre_archivo, key):
		
		with open(nombre_archivo, 'rb') as fo:
			texto_plano = fo.read()
		enc = self.cifrar(texto_plano, key)
	
		nombre_archivo =str(nombre_archivo).split('.')
		with open(nombre_archivo[0] + "-encDES."+nombre_archivo[1], 'wb') as fo:
			fo.write(enc)
			

	def descifrar_archivo(self, nombre_archivo, key):
		
		with open(nombre_archivo, 'rb') as fo:
			ciphertext = fo.read()
		dec = self.descifrar(ciphertext, key)
		nombre_archivo =str(nombre_archivo).split('.')
		with open(nombre_archivo[0] + "-desDES."+nombre_archivo[1], 'wb') as fo:
			fo.write(dec)