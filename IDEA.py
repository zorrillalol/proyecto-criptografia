from Crypto import Random

import os
import os.path
from os import listdir
from os.path import isfile, join
import time
import binascii

class IDEA:
	"""docstring for IDEA"""
	key=''
	
	def cifrar(self, message, key, key_size=128):
		message = self.pad(message)
		iv = Random.new().read(8)
		key = key.encode("utf-8")
		cipher = IDEA(binascii.hexlify(key))
		return iv + cipher.encrypt(message)
	
	def descifrar(self, ciphertext, key):
		iv = ciphertext[:8]
		key = key.encode("utf-8")
		cipher = IDEA.new(binascii.hexlify(key))
		plaintext = cipher.decrypt(ciphertext[8:])
		return plaintext.rstrip(b"\0")


	def pad(self,s):
		return s+b"\0" * (8 - len(s) % 8)

	def cifrar_archivo(self, nombre_archivo, key):
		
		with open(nombre_archivo, 'rb') as fo:
			texto_plano = fo.read()
		enc = self.cifrar(texto_plano, key)
	
		nombre_archivo =str(nombre_archivo).split('.')
		with open(nombre_archivo[0] + "-encIDEA."+nombre_archivo[1], 'wb') as fo:
			fo.write(enc)
			

	def descifrar_archivo(self, nombre_archivo, key):
		
		with open(nombre_archivo, 'rb') as fo:
			ciphertext = fo.read()
		dec = self.descifrar(ciphertext, key)
		nombre_archivo =str(nombre_archivo).split('.')
		with open(nombre_archivo[0] + "-desIDEA."+nombre_archivo[1], 'wb') as fo:
			fo.write(dec)
			
