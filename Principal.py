from DES import DES
from AES_128 import AES_128
from IDEA import IDEA


def main():
	print('empezando')
	des = DES()
	key = 'ejemplok'
	des.cifrar_archivo('C:\\Users\\luis5\\Documents\\JoseLuis\\Universidad\\8°Semestre\\Criptografia\\proyecto_final_librerias\\hola.txt',key)
	des.descifrar_archivo('C:\\Users\\luis5\\Documents\\JoseLuis\\Universidad\\8°Semestre\\Criptografia\\proyecto_final_librerias\\hola-encDES.txt',key)

	aes = AES_128()
	aes.cifrar_archivo('C:\\Users\\luis5\\Documents\\JoseLuis\\Universidad\\8°Semestre\\Criptografia\\proyecto_final_librerias\\hola.txt','abcdefghijklmnop')
	aes.descifrar_archivo('C:\\Users\\luis5\\Documents\\JoseLuis\\Universidad\\8°Semestre\\Criptografia\\proyecto_final_librerias\\hola-encAES.txt','abcdefghijklmnop')

	idea = IDEA()
	idea.cifrar_archivo('C:\\Users\\luis5\\Documents\\JoseLuis\\Universidad\\8°Semestre\\Criptografia\\proyecto_final_librerias\\hola.txt','abcdefghijklmnop')
	idea.descifrar_archivo('C:\\Users\\luis5\\Documents\\JoseLuis\\Universidad\\8°Semestre\\Criptografia\\proyecto_final_librerias\\hola-encIDEA.txt','abcdefghijklmnop')


	
if __name__ == '__main__':
	main()