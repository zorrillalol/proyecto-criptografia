from Crypto import Random
from Crypto.Cipher import AES
import os
import os.path
from os import listdir
from os.path import isfile, join
import time
import binascii

class AES_128:
	"""docstring for AES"""
	key=''
	
	def cifrar(self, message, key, key_size=128):
		message = self.pad(message)
		iv = Random.new().read(AES.block_size)
		key = key.encode("utf-8")
		cipher = AES.new(binascii.hexlify(key), AES.MODE_CBC, iv)
		return iv + cipher.encrypt(message)
	
	def descifrar(self, ciphertext, key):
		iv = ciphertext[:AES.block_size]
		key = key.encode("utf-8")
		cipher = AES.new(binascii.hexlify(key), AES.MODE_CBC, iv)
		plaintext = cipher.decrypt(ciphertext[AES.block_size:])
		return plaintext.rstrip(b"\0")


	def pad(self,s):
		return s+b"\0" * (AES.block_size - len(s) % AES.block_size)

	def cifrar_archivo(self, nombre_archivo, key):
		
		with open(nombre_archivo, 'rb') as fo:
			texto_plano = fo.read()
		enc = self.cifrar(texto_plano, key)
	
		nombre_archivo =str(nombre_archivo).split('.')
		with open(nombre_archivo[0] + "-encAES."+nombre_archivo[1], 'wb') as fo:
			fo.write(enc)
			

	def descifrar_archivo(self, nombre_archivo, key):
		
		with open(nombre_archivo, 'rb') as fo:
			ciphertext = fo.read()
		dec = self.descifrar(ciphertext, key)
		nombre_archivo =str(nombre_archivo).split('.')
		with open(nombre_archivo[0] + "-desAES."+nombre_archivo[1], 'wb') as fo:
			fo.write(dec)
			
